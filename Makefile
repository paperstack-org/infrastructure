.PHONY: clean known_hosts ssh_key

all: ssh_key known_hosts

clean:
	rm microcloud_*

known_hosts:
	ssh-keyscan paperstack.org > microcloud_known_hosts 2> /dev/null
	ssh-keyscan 172.16.0.10 >> microcloud_known_hosts 2> /dev/null
	ssh-keyscan 172.16.0.20 >> microcloud_known_hosts 2> /dev/null
	ssh-keyscan 172.16.0.30 >> microcloud_known_hosts 2> /dev/null
	ssh-keyscan 172.16.0.40 >> microcloud_known_hosts 2> /dev/null
	ssh-keyscan 172.16.0.50 >> microcloud_known_hosts 2> /dev/null

ssh_key:
	ssh-keygen -t rsa -b 8192 -C microcloud-ci -P "" -f microcloud_id_rsa
