#!/bin/bash
if [ "$2" = "" ]; then
  echo "Waiting for state to be consistent ..."
else
  echo "Waiting for $2 ..."
fi

COMMAND=$1
RETURN_CODE=1
while [ $RETURN_CODE -ne 0 ]; do
  RETURN_CODE=$(eval "$COMMAND; echo \$?;")
  echo -en "\rWaited for: ${SECONDS}s"
  sleep 1
done
echo

# Allow changes to propagate.
sleep 5
