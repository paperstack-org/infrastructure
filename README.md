# Microcloud

The infrastructure configuration for a Kubernetes microcloud.

## Dashboards

To monitor some of the components, the following webinterfaces are available:

- Edgerouter: https://edgerouter.internal.k8s.paperstack.org
- Rook Ceph: https://ceph.internal.k8s.paperstack.org
- Kubernetes: https://dashboard.internal.k8s.paperstack.org
- Traefik: https://traefik.internal.k8s.paperstack.org/dashboard/

All dashboards are protected by an OAuth 2.0 proxy to ensure that only specific Google-authenticated users can access them.

## Ansible

During the installation of the **K3S** cluster via Ansible some secret variables are passed via the environment. The file has the following structure:

```yaml
export ROUTER_PASSWORD_HASH='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

export ROOT_DOMAIN=example.com

export KUBE_API_URL=api.k8s.example.com
export KUBE_API_ISSUER_URL=https://accounts.google.com
export KUBE_API_CLIENT_ID=xxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com
export KUBE_API_CLIENT_SECRET=
export KUBE_API_USERNAME_CLAIM=email

export POMERIUM_CLIENT_ID=xxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com
export POMERIUM_CLIENT_SECRET=
export POMERIUM_SHARED_SECRET=
export POMERIUM_COOKIE_SECRET=
export POMERIUM_CLUSTER_ADMIN_TOKEN=xxxxxxxxxxxxxxx.xxxxxxxxxxxxxxx.xxxxxxxxxxxxxxx

export MINIO_
```

## Servers

The list below describes the available devices for application deployments:

- Router:

  | Bootstrapping | Hostname       | Hardware              | OS               | IPs                                                  |
  | ------------- | -------------- | --------------------- | ---------------- | ---------------------------------------------------- |
  | Manual        | `edgerouter-0` | Ubiquity EdgeRouter X | Ubiquity Edge OS | `212.112.129.219`<br/>`172.16.0.1`<br/>`192.168.0.1` |

  The router provides NAT, DHCP and other services that are required for basic network operation. It also acts as a bastion host for SSH and forwards ports to application servers.

- Kubernetes control plane:

  | Bootstrapping | Hostname      | Hardware              | OS                                        | IPs           |
  | ------------- | ------------- | --------------------- | ----------------------------------------- | ------------- |
  | Manual        | `odroidhc2-0` | Hardkernel Odroid HC2 | [Armbian Focal Server][armbian odroidhc2] | `172.16.0.20` |

  The Kubernetes control plane runs the Kubernetes API server. It manages, schedules and coordinates workloads. Depending on its configuration it may also run workloads.

- Kubernetes worker:

  | Bootstrapping | Hostname      | Hardware                         | OS                                   | IPs           |
  | ------------- | ------------- | -------------------------------- | ------------------------------------ | ------------- |
  | Manual        | `workhorse-0` | Intel i3-7100@3.9GHz, 16GB DDR4  | [Ubuntu Focal Server][ubuntu server] | `172.16.0.10` |
  | Manual        | `rockpix-0`   | Intel x5-Z8350@1.44GHz, 4GB DDR3 | [Ubuntu Focal Server][ubuntu server] | `172.16.0.30` |

  The Kubernetes worker is dedicated to running scheduled workloads based on the information it gets from the Kubernetes API.

## Bootstrapping

### Armbian Focal

1. Flash the firmware image.
1. Find the device on the network.
1. Connect via SSH with the credentials `root` and `1234`.
1. Change the password via the prompt.
1. Follow the instructions to create a user account `ubuntu`.
1. Configure passwordless `sudo`: `echo "$(whoami) ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/ubuntu`
1. Update the system: `sudo apt update && sudo apt upgrade -y`
1. Install the root filesystem to an SSD: `sudo nand-sata-install`
1. Configure `sshync` to synchronize SSH keys:

   ```bash
   # Download binary.
   $ sudo curl -sSL https://gitlab.com/nicklasfrahm/sshync/-/jobs/artifacts/main/raw/sshync-arm?job=compile-arm -o /bin/sshync
   # Make binary executable.
   $ sudo chmod +x /bin/sshync
   # Install sshync as background daemon.
   $ sudo sshync install
   # Configure sshync.
   $ sudo vi /etc/sshync/config.yaml
   ```

   The config should then look similar to this:

   ```yaml
   credentials:
     - user: ubuntu
       ssh_keys:
         - provider: github
           username: nicklasfrahm
         - provider: gitlab
           username: nicklasfrahm
   daemon:
     poll_interval:
       seconds: 300
   ```

1. Configure the OpenSSH server (`sudo vi /etc/ssh/sshd_config`), such that the following 4 options are set:

   ```txt
   PubkeyAuthentication yes
   ChallengeResponseAuthentication no
   PasswordAuthentication no
   PermitRootLogin no
   UsePAM no
   UseDNS no
   ```

1. Restart the OpenSSH server: `sudo systemctl restart ssh`
1. Configure network: `sudo armbian-config`

### Disks

To prepare disks for Rook Ceph, you need to wipe them:

```shell
$ curl -sL https://gitlab.com/paperstack-org/microcloud/-/raw/main/scripts/reset_disk.sh -o - | DISK=sdi TYPE=ssd bash
```

[armbian odroidhc2]: https://www.armbian.com/odroid-hc1/
[ubuntu server]: https://ubuntu.com/download/server
