#!/usr/bin/env bash
DEVICE="/dev/$DISK"

# Check if variables are specified.
if [ "$DEVICE" = "/dev/" ]; then
  echo -e "Please provide a disk in the format: DISK=sdb"
  exit 1
fi
if [ "$TYPE" = "" ]; then
  echo -e "Please provide the disk type (hdd, ssd) in the format: TYPE=hdd"
  exit 1
fi

# Zap the disk to a fresh, usable state (zap-all is important, because the MBR has to be clean).
# You will have to run this step for all disks.
sgdisk --zap-all $DEVICE

if [ "$TYPE" = "hdd" ]; then
  # Clean hdds with dd.
  dd if=/dev/zero of="$DEVICE" bs=1M count=100 oflag=direct,dsync
fi

if [ "$TYPE" = "ssd" ]; then
  # Clean disks such as ssd with blkdiscard instead of dd.
  blkdiscard $DEVICE
fi
